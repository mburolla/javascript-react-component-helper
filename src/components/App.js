import { Child1 } from './Child1';
import { Child2 } from './Child2';
import { Child3 } from './Child3';
import { Child4 } from './Child4';
import { Parent1 } from './Parent1';
import { GetTime } from '../util/time';

function App() {

  console.log("App: " + GetTime());

  const handleClick = () => {
    console.log('App: Clicked: ' + GetTime())
  }

  return (
    <div className="App">
        <Child1 personId="1" catId="2" dogId="3"/>
        <Child2 />
        <Child3 />
        <Child4 />
        <Parent1 />
        <button onClick={handleClick}>Push me</button>
    </div>
  );
}

export default App;
