import React from 'react';
import { GetTime } from '../util/time';

export class Child4 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            catId: 0,
            personId: 0
        };
        
        console.log('Child4: ' + GetTime());
    }

    render() {
        console.log('Child4: Render ' + GetTime());
        return (
            <div>
               Child4 - personId {this.state.personId}: catId: {this.state.catId}
            </div>
        );
    }

    componentDidMount() {
        //this.setState({catId: 1}); // Causes an infinite loop.
        console.log('Child4: Component Did Mount ' + GetTime());
    }
}
