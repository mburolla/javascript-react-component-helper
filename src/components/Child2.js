import React from 'react';
import { useState, useEffect } from 'react';
import { GetTime } from '../util/time';

export const Child2 = (props) => {
    const [personId, setPersonId] = useState(0);
    const [catId, setCatId] = useState(0);

    console.log('Child2: ' + GetTime());

    useEffect(() => {
        console.log('Child2: UseEffect: ' + GetTime())
    },[personId, catId]); // useEffect() is fired when personId or catId has been changed.

    return (
        <div>Child2 - personId: {personId} catId: {catId}</div>
    );
};
