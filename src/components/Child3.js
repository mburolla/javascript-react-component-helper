import React from 'react';
import { useState, useEffect } from 'react';
import { GetTime } from '../util/time';

export const Child3 = (props) => {
    const [personId, setPersonId] = useState(0);
    const [catId, setCatId] = useState(0);
    let dogId = 0;

    console.log('Child3: ' + GetTime());

    useEffect(() => {
        console.log('Child3: UseEffect: ' + GetTime())
    }); // useEffect() is fired all the time.

    return (
        <div>Child3 - personId: {personId} catId: {catId}</div>
    );
};
