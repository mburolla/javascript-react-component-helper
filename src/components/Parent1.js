import React from 'react';
import { Child5 } from './Child5';
import { Child6 } from './Child6';
import { useState, useEffect } from 'react';
import { GetTime } from '../util/time';

export const Parent1 = () => {
    const [personId, setPersonId] = useState(0);

    console.log("Parent1: " + GetTime())

    useEffect(() => {
        console.log("Parent1: UseEffect " + GetTime())
    }, []); // [] only loaded after the component has been mounted into the DOM (a page refresh).

    return (
        <div>
            Parent1 (Child5, Child6)
            < Child5 />
            < Child6 />
        </div>
    );
};
