import React from 'react';
import { useState, useEffect } from 'react';
import { GetTime } from '../util/time';

export const Child5 = () => {
    const [personId, setPersonId] = useState(0);

    console.log("Child5: " + GetTime())

    useEffect(() => {
        console.log("Child5: UseEffect " + GetTime())
    }, []); // [] only loaded after the component has been mounted into the DOM (a page refresh).

    return (
        <div>
            Child5
        </div>
    );
};
