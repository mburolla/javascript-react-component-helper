import React from 'react';
import { useState, useEffect } from 'react';
import { GetTime } from '../util/time';

export const Child1 = (props) => {
    const [personId, setPersonId] = useState(0);
    const [catId, setCatId] = useState(0);
    let [counter1, setCounter1] = useState(0);
    let counter2 = 0;
    let doggieId = props.dogId;

    const handleClick = () => {
        setCounter1(++counter1);
        ++counter2;
        console.log('Child1: Clicked: Counter: ' + counter1 + ' ' + counter2 + ' ' + GetTime())
    }

    // Clicking the button invokes the Child1() function.
    console.log('Child1: ' + GetTime());

    // setPersonId(props.personId); // Causes an infinite loop with useEffect().

    useEffect(() => {
        console.log('Child1: UseEffect: ' + GetTime())
    },[props.personId]); // useEffect() is fired ONLY when personId has been changed.

    return (
        <div>Child1 - personId: {personId} catId: {catId} dogId: {doggieId} 
            <button onClick={handleClick}>Push me</button>
        </div>
    );
};
