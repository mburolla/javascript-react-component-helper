import React from 'react';
import { useState, useEffect } from 'react';
import { GetTime } from '../util/time';

export const Child6 = () => {
    const [personId, setPersonId] = useState(0);
    
    console.log("Child6: " + GetTime())

    useEffect(() => {
        console.log("Child6: UseEffect " + GetTime())
    }, []); // [] only loaded after the component has been mounted into the DOM (a page refresh).

    return (
        <div>
            Child6
        </div>
    );
};
