# JavaScript React Component Helper
A simple React application that demonstrates:
 - Rendering
 - State
 - `useEffect()`
 - Functional and Class based Components

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the app: `npm start`

# Notes
- If the state of a parent changes, the parent and all the children are re-rendered
